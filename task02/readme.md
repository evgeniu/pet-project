# Task: write a Guess the Number game using the Bash script.
## Subtask: Create a random number generator script.
script.bash: Generate numbers

readme.md: Add commit headers

Subtask completed.


## Subtask: Extract Function refactoring.
script.bash: Refactor

readme.md: Add commit headers

Subtask completed.


## Subtask: Implement comparison of the randomly generated number X with the number Y.
script.bash: Number comparison

readme.md: Add commit headers

Subtask completed.