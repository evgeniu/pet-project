#! /bin/bash

number=$RANDOM%10+1

if [[ $1 -gt $number  ]]; then
	echo $1 is greater than $((number))
elif [[ $1 -lt $number ]]; then
	echo $1 is less than $((number))
else
	echo $1 is equal than $((number))
fi
